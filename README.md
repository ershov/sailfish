# sailfish

sailfish it's a JavaScript framework consisting of server-side and client-side. With sailfish you can quickly create an maintainable application of any complexity


## frontend

Frontend часть приложения, разрабатываемая с помощью sailfish, организована с использованием подхода [Asynchronous Module Definition](https://github.com/amdjs/amdjs-api/wiki/AMD)
Для организации модульной структуры приложения используется библиотека [require.js](http://requirejs.org/)

### Компоненты

Компонент - самостоятельная сущность, совокупность которых организует приложение. Компонент может быть составным,
т.е состоящим из других компонентов, и управляющим их поведением.

Компонент не обязан быть визуальным. Он может быть просто js модулем, предоставляющим некий функционал.

Для описания компонента можно использовать следующие технологии:
 - JavaScript
 - CSS (LESS)
 - HTML

Обязательным пунктом является только JavaScript.






Таким образом
